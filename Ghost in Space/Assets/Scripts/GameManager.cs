﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum gameStatus
{
   initial, next, play, gameover, win
}

public class GameManager :  Singleton<GameManager>
{
    [SerializeField]
    private int totalWaves = 3;
    [SerializeField]
    private int totalLives = 3;
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private GameObject enemy;
    [SerializeField]
    private int spawnDelay=2;
    [SerializeField]
    private float spawnDistance = 12f;
    [SerializeField]
    private Button playBtn;
    [SerializeField]
    private Text playBtnLbl;
    [SerializeField]
    private int[] WaveEnemiesNumber;
    [SerializeField]
    private Text lblStats1;
    [SerializeField]
    private Text lblStats2;
    [SerializeField]
    private int deathpenalty = 50;

    private int maxEnemiesSpawn;
    private int lives;
    private GameObject playerInstance;
    private Player playerObject;
    private int waveNumber;
    private int enemiesSpawn = 0;
    private int enemiesKilled = 0;
    private int enemiesWaveKilled = 0;
    private gameStatus currentState = gameStatus.initial;
    private bool GamePaused;
    private int score;
    

    public List<Enemy> EnemyList = new List<Enemy>();


    public GameObject PlayerInstance
    {
        get
        {
            return playerInstance;
        }
    }
    public int EnemiesKilled
    {
        get
        {
            return enemiesKilled;
        }
        set
        {
            enemiesKilled = value;
        }
    }
    public int EnemiesWaveKilled
    {
        get
        {
            return enemiesWaveKilled;
        }
        set
        {
            enemiesWaveKilled = value;
        }
    }
    void Start()
    {
        GamePaused = true;
        playBtn.gameObject.SetActive(true);
        lblStats1.gameObject.SetActive(false);
        lblStats2.gameObject.SetActive(false);
    }



    public void SpawnPlayer()
    {
        Vector3 pos = new Vector3(0, 0, player.transform.position.z);
        playerInstance = (GameObject)Instantiate(player, pos, Quaternion.identity);
        playerObject = playerInstance.GetComponent<Player>();
        lives--;
    }


    public void PlayerDies()
    {
        Destroy(playerInstance);
        playerInstance = null;
        score -= deathpenalty;
        if (score < 0)
            score = 0;
        if (lives > 0)
        {
            SpawnPlayer();
        }
        else
        {
            currentState = gameStatus.gameover;
            GamePaused = true;
            DestroyAllEnemies();
            DisplayLablMessage();
            playBtn.gameObject.SetActive(true);
        }
        DisplayStats();
    }


    public IEnumerator SpawnEnemy()
    {
        if(enemiesSpawn < maxEnemiesSpawn && GamePaused == false)
        {
            Vector3 pos = new Vector3(0, 0, 0);
            Vector3 offset = Random.onUnitSphere;
            offset.z = 0;
            offset = offset.normalized * spawnDistance;
            GameObject enem = Instantiate(enemy, pos+ offset, Quaternion.identity);
            enemiesSpawn += 1;
            Enemy Enemy = enem.GetComponent <Enemy>();
            RegisterEnemy(Enemy);
        }

        yield return new WaitForSeconds(spawnDelay);
        StartCoroutine(SpawnEnemy());
    }

    public void RegisterEnemy(Enemy enemy)
    {
        EnemyList.Add(enemy);
    }

    public void UnregisterEnemy(Enemy enemy)
    {
        EnemyList.Remove(enemy);
        Destroy(enemy.gameObject);
    }

    public void DestroyAllEnemies()
    {
        foreach (Enemy enemy in EnemyList)
        {
            Destroy(enemy.gameObject);
        }

        EnemyList.Clear();
    }

    void OnGUI()
    {
        if (currentState == gameStatus.initial) return;
        if (currentState == gameStatus.win)
        {
            GUI.Label(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 25, 100, 50), "You Won!!");
            return;
        }
        if (currentState == gameStatus.gameover)
        {
            GUI.Label(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 25, 100, 50), "Game Over!!");
            return;
        }
    }

    private void CleanUpAll()
    {
        DestroyAllEnemies();
        if(playerInstance != null)
        {
            Destroy(PlayerInstance);
            playerInstance = null;
        }
    }

    private void StartGame()
    {
        enemiesSpawn = 0;
        waveNumber = 0;
        enemiesKilled = 0;
        enemiesWaveKilled = 0;
        score = 0;
        lives = totalLives;
        ApplyEnemiesNumber();
        SpawnPlayer();
        StartCoroutine(SpawnEnemy());
        DisplayStats();
    }

    public void playBtnPressed()
    {
        switch (currentState)
        {
            case gameStatus.initial:
                CleanUpAll();
                StartGame();
                DisplayStats();
                lblStats1.gameObject.SetActive(true);
                lblStats2.gameObject.SetActive(true);
                currentState = gameStatus.next;
                break;
            case gameStatus.next:
                playerInstance.SetActive(true);
                waveNumber += 1;
                enemiesSpawn = 0;
                enemiesWaveKilled = 0;
                ApplyEnemiesNumber();
                DisplayStats();
                break;
            case gameStatus.win:
            case gameStatus.gameover:
                CleanUpAll();
                StartGame();
                currentState = gameStatus.next;
                break;
        }
        playBtn.gameObject.SetActive(false);
        GamePaused = false;
    }

    private void DisplayLablMessage()
    {
        switch (currentState)
        {
            case gameStatus.next:
                playBtnLbl.text = "Start Next Wave?";
                break;
            case gameStatus.gameover:
            case gameStatus.win:
                playBtnLbl.text = "Play Again?";
                break;
        }
    }

    public void IsWaveOver()
    {
        if (enemiesWaveKilled == maxEnemiesSpawn)
        {
            GamePaused = true;
            if (waveNumber == WaveEnemiesNumber.Length - 1)
                currentState = gameStatus.win;
            else
            {
                currentState = gameStatus.next;
            }
            DisplayLablMessage();
            playBtn.gameObject.SetActive(true);
            playerInstance.SetActive(false);
        }
        DisplayStats();
    }

    private void ApplyEnemiesNumber()
    {
        if(waveNumber < WaveEnemiesNumber.Length)
        {
            maxEnemiesSpawn = WaveEnemiesNumber[waveNumber];
            enemiesSpawn = 0;
        }
    }

    public void DisplayStats()
    {
        if(PlayerInstance != null)
            lblStats1.text = string.Format("Lives: {0} Health: {1} Score: {2:n0}", lives+1, playerObject.HealthPoints, score);
        else
            lblStats1.text = string.Format("Lives: {0} Health: {1} Score: {2:n0}", 0, 0, score);

        lblStats2.text = string.Format("Wave: {0}/{1} Enemies Remain: {2}", waveNumber + 1, WaveEnemiesNumber.Length, maxEnemiesSpawn - enemiesWaveKilled);
    }
    public void addMoney()
    {
        score += 10 * (1 + waveNumber);
        Debug.LogFormat("Score = {0}", score);
    }
}
