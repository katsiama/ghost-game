﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerShooting : MonoBehaviour {
    public Vector3 laserOffset = new Vector3(0, 0.5f, 0);

    public GameObject laserPrefab;
    int laserPrefabLayer;

    public float fireDelay = 0.25f;
    float cooldownTimer = 0;

     void Start()
    {
        laserPrefabLayer = gameObject.layer;
    }
    
	// Update is called once per frame
	void Update () {
        cooldownTimer -= Time.deltaTime;

       // if (Input.GetButtonDown("Fire1")  && cooldownTimer <= 0)
        if (CrossPlatformInputManager.GetButton("Shoot") && cooldownTimer <= 0)
            {
            //SHOOT!
            cooldownTimer = fireDelay;

            Vector3 offset = transform.rotation * laserOffset;
            GameObject bulletGo = (GameObject)Instantiate(laserPrefab, transform.position + offset, transform.rotation);
            bulletGo.layer = laserPrefabLayer;
        }
	
	}
}
