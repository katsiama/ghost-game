﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    [SerializeField]
    private int healthPoints;

    [SerializeField]
    public int HealthPoints
    {
        get
        {
            return healthPoints;
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Trigger!");
        if (other.tag == "ground")
        {
            Debug.Log("Ground collider");
            return;
        }
        if(other.tag == "Bullet")
        {
            Bullet newP = other.gameObject.GetComponent<Bullet>();
            Debug.LogFormat("Enemy Hit Bullet {0}", newP.AttackStrength);
            if (newP.BulletType == BulletType.Enemy)
            {
                playerHit(newP.AttackStrength);
                Destroy(other.gameObject);
            }
        }
        else if(other.tag == "Enemy")
        {
            Enemy newP = other.gameObject.GetComponent<Enemy>();
            playerHit(newP.DamageOnCollision);
        }
    }

    public void playerHit(int hitpoints)
    {
        if (healthPoints - hitpoints > 0)
        {
            healthPoints -= hitpoints;
            GameManager.Instance.DisplayStats();
            //anim.Play("Hurt");
        }
        else
        {
            //anim.SetTrigger("dodie");
            healthPoints = 0;
            die();
        }
    }

    public void die()
    {
       // enemyCollider.enabled = false;
        GameManager.Instance.PlayerDies();
        // isDead = true;

        //Destroy(gameObject);
        // GameManager.Instance.UnregisterEnemy(this);
        // GameManager.Instance.TotalKilled += 1;
        // GameManager.Instance.addMoney(rewardAmt);
        // GameManager.Instance.isWaveOver();
    }
}
