﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMovement : MonoBehaviour {

   public float maxSpeed = 5f;
   public float rotSpeed = 180f;

   public float ghostBoundaryRadius = 1f;
	void Start () {
	
	}
	
	void Update () {
        //grab our rotation quaternion
        Quaternion rot = transform.rotation;

        //Grab the z euler angle
        float z = rot.eulerAngles.z;
        //Change the z angle based on input
        //z -= Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;
        z -= CrossPlatformInputManager.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;
        //z -= Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;

        //Recreate the quaternion
        rot = Quaternion.Euler(0, 0, z);

        //Feed the quaternion into our rotation 
        transform.rotation = rot;


        //Move the ghost
        Vector3 pos = transform.position;
     

        Vector3 velocity = new Vector3(0, CrossPlatformInputManager.GetAxis("Vertical") * maxSpeed * Time.deltaTime);

        pos += rot * velocity;

        //Restrict the player at the camera's map

        //first to vertical
        if (pos.y + ghostBoundaryRadius > Camera.main.orthographicSize)
        {
            pos.y = Camera.main.orthographicSize - ghostBoundaryRadius;
        }
        if (pos.y - ghostBoundaryRadius < -Camera.main.orthographicSize)
        {
            pos.y = -Camera.main.orthographicSize + ghostBoundaryRadius;
        }

        //Now calculate the orthographicsize

        float screenRatio = (float)Screen.width / (float)Screen.height;
        float widthOrtho = Camera.main.orthographicSize * screenRatio;

        //Horizontal bounds
        if (pos.x + ghostBoundaryRadius >widthOrtho)
        {
            pos.x = widthOrtho - ghostBoundaryRadius;
        }
        if (pos.x - ghostBoundaryRadius < -widthOrtho)
        {
            pos.x = -widthOrtho + ghostBoundaryRadius;
        }

        //Update the position
        transform.position = pos;
     
	}
}
