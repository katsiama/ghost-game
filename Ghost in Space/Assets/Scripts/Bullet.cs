﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BulletType
{
    Player,
    Enemy
};

public class Bullet : MonoBehaviour {

    [SerializeField]
    private int attackStrength = 1;
    [SerializeField]
    private BulletType bulletType;

    public int AttackStrength
    {
        get
        {
            return attackStrength;
        }
    }

    public BulletType BulletType
    {
        get
        {
            return bulletType;
        }
    }

    void FixedUpdate()
    {
        float screenRatio = (float)Screen.width / (float)Screen.height;
        float widthOrtho = Camera.main.orthographicSize * screenRatio;
        Vector3 pos = transform.position;
        bool bdie = false;
        if (pos.x > widthOrtho || pos.x < -widthOrtho)
            bdie = true;
        if (pos.y > Camera.main.orthographicSize || pos.y < -Camera.main.orthographicSize)
            bdie = true;
        if (bdie)
        {
            Debug.Log("Destroy bullet");
            Destroy(gameObject);
        }
    }
}
