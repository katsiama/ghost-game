﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoving : MonoBehaviour {

    [SerializeField]
    public float navigationUpdate=0.1f;

    private float navigationTime = 0;

    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        GameObject player = GameManager.Instance.PlayerInstance;
        if (player == null)
            return;
        navigationTime += Time.deltaTime;
        if (navigationTime > navigationUpdate)
        {
            if (System.Math.Abs(Vector3.Distance(transform.position, player.transform.position)) > 4f)
                transform.position = Vector2.MoveTowards(transform.position, player.transform.position, navigationTime);

            navigationTime = 0;
        }

    }
    void FixedUpdate()
    {
        GameObject player = GameManager.Instance.PlayerInstance;
        if (player == null)
            return;

    }
}
