﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    [SerializeField]
    private int healthPoints = 1;
    [SerializeField]
    private int damageOnCollision = 1;

    private Transform enemy;
    private Collider2D enemyCollider;

    public int DamageOnCollision
    {
        get
        {
            return damageOnCollision;
        }
    }

    // Use this for initialization
    void Start ()
    {
        enemy = GetComponent<Transform>();
        enemyCollider = GetComponent<Collider2D>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Bullet")
        {
            Bullet newP = other.gameObject.GetComponent<Bullet>();
            Debug.LogFormat("Enemy Hit Bullet {0}", newP.AttackStrength);
            enemyHit(newP.AttackStrength);
            Destroy(other.gameObject);
        }
        else if(other.tag == "Player")
        {
            enemyHit(healthPoints);
        }
    }
    public void enemyHit(int hitpoints)
    {
        if (healthPoints - hitpoints > 0)
        {
            healthPoints -= hitpoints;
            //anim.Play("Hurt");
        }
        else
        {
            //anim.SetTrigger("dodie");
            die();
        }
    }

    public void die()
    {
       // isDead = true;
        enemyCollider.enabled = false;
        Destroy(gameObject);
        GameManager.Instance.UnregisterEnemy(this);
        GameManager.Instance.EnemiesKilled += 1;
        GameManager.Instance.EnemiesWaveKilled += 1;
        GameManager.Instance.addMoney();
        GameManager.Instance.IsWaveOver();
    }
}
